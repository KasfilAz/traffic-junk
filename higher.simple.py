import asyncio

from selenium import webdriver

from proxies import proxies as proxy_lists

URL = "http://mazsupertop.blogspot.com"


async def set_preferences(ip_addr, port):
    profile = webdriver.FirefoxProfile()
    profile.set_preference("network.proxy.type", 1)
    profile.set_preference("network.proxy.socks", ip_addr)
    profile.set_preference("network.proxy.socks_port", port)
    profile.set_preference("network.proxy.socks_version", 5)
    return profile


async def get_browser(proxy):
    ip_addr = proxy[0]
    port = proxy[1]
    profile = await set_preferences(ip_addr, port)
    browser = webdriver.Firefox(profile)
    print("browsing with %s: %i" % (ip_addr, port))
    browser.get(URL)
    browser.quit()


async def visit_web():
    demand = 0
    proxy_wait = []
    proxy_count = len(proxy_lists)
    print("found %i proxies" % proxy_count)
    for i in range(proxy_count):
        if (demand < 5 and i <= proxy_count):
            proxy_wait.append(proxy_lists[i])
            demand = demand + 1
        else:
            await asyncio.gather(*(get_browser(proxy) for proxy in proxy_wait))
            proxy_wait = []
            demand = 0


if __name__ == "__main__":
    asyncio.run(visit_web())
